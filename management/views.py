
# Create your views here.
from rest_framework.response import Response
from rest_framework.decorators import api_view
from publish import models
from django.forms.models import model_to_dict
from django.http import HttpResponse,JsonResponse
import json
import datetime

@api_view(['POST'])
def checkpic(requset):

    fid = requset.data.get('fid')
    check = requset.data.get('check')
    try:
        obj = models.PublishForm.objects.get(fid = fid)
        if check:
            obj.status = check
            obj.save()
        else:
            obj.status = -1
            obj.save()
        return Response({"success":True})
    except:
        return Response({"success":False})

class DateEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj,datetime.datetime):
            return obj.strftime("%Y-%m-%d %H:%M:%S")
        else:
            return json.JSONEncoder.default(self,obj)

@api_view(['GET'])
def checklist(requset):
    model = models.PublishForm.objects.all().filter(status=0)
    json_list = []
    #data = serializers.serialize('json', models.PublishForm.objects.all(), fields=('fid', 'content'))
    #print(data)

    for i in model:
        json_dict = model_to_dict(i)
        json_dict["time"] = i.time
        del json_dict["ip"]
        pics_set = models.Picture.objects.all().filter(fid=i.fid)
        pics = []
        for j in pics_set:
            pics.append(str(j.pic))
        json_dict['pic'] = pics
        # json_dict["pic"] = pics
        json_list.append(json_dict)

    return HttpResponse(json.dumps(json_list,cls=DateEncoder))

@api_view(['GET'])
def pic(requset):
    pid = requset.GET.get('pid')
    try:
        model = models.Picture.objects.get(pid = pid)
        data = str(model.pic)
        return HttpResponse(data)
    except:
        print("没有这张图")
        return Response({},status=404)


@api_view(['GET'])
def get_item(requset):
    fid = requset.GET.get("fid")
    try:
        model = models.PublishForm.objects.all().get(fid = fid)
        json_dict = model_to_dict(model)
        json_dict["time"] = model.time
        pics_set = models.Picture.objects.all().filter(fid=fid)
        pics = []
        for j in pics_set:
            pics.append(str(j.pic))
        json_dict['pics'] = pics
        return  HttpResponse(json.dumps(json_dict,cls=DateEncoder))
    except:
        return Response({},status=404)


@api_view(['GET'])
def thumbpic(request):
    pid = request.GET.get("pid")
    try:
        model = models.Thumb.objects.all().get(pic = pid)
        return Response(str(model.thumb))
    except:
        return Response({},status=404)



@api_view(['GET'])
def all(requset):
    try:
        page = requset.GET.get("page")
    except:
        page = 1
    model = models.PublishForm.objects.all()
    json_list = []
    data = {}
    #data = serializers.serialize('json', models.PublishForm.objects.all(), fields=('fid', 'content'))
    #print(data)

    for i in model:
        json_dict = model_to_dict(i)
        json_dict["time"] = i.time
        pics_set = models.Picture.objects.all().filter(fid=i.fid)
        pics = []
        for j in pics_set:
            pics.append(str(j.pic))
        json_dict['pic'] = pics
        # json_dict["pic"] = pics
        json_list.append(json_dict)
    size = len(json_dict)
    count = size//10
    if(size%10!=0):
        count = count +1
    data["count"]=count
    if page == "1":
        data["previous"] = ""
    if page == "2":
        data["previous"]="http://127.0.0.1:8000/api/list/all/"
    print(page)
    next_page = int(page) +1
    print(next_page)
    next = 'http://127.0.0.1:8000/api/list/all/?page='+str(next_page)
    data["next"] = next
    data["results"] = json_list[(int(page)-1)*10:int(page)*10]
    print(data)
    return HttpResponse(json.dumps(data,cls=DateEncoder))