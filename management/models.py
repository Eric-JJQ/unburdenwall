from django.db import models

# Create your models here.
from publish.models import PublishForm


class GeneratedPic(models.Model):
    gid = models.AutoField(primary_key=True)
    form = models.ForeignKey(to=PublishForm, on_delete=models.CASCADE)
    pic = models.ImageField(upload_to='pics/gen/')
    time = models.DateTimeField(auto_now_add=True)
    send = models.BooleanField(default=False)

    class Meta:
        db_table = "GeneratedPic"

    def __str__(self):
        return str(self.form)
