
from django.urls import path
from management import views as management_views

urlpatterns = [
    path('check', management_views.checkpic),
    path('checklist', management_views.checklist),
    path('pic', management_views.pic),
    path('item', management_views.get_item),
    path('thumbpic', management_views.thumbpic),
    path('all', management_views.all),

]

