from django.http import JsonResponse
# Create your views here.
from rest_framework import viewsets
from rest_framework.decorators import api_view
from rest_framework.permissions import AllowAny
from rest_framework.viewsets import ModelViewSet

from publish.models import PublishForm, Picture
from publish.serializers import FormMakeSerializer, PictureSerializer


@api_view(['GET'])
def FormPage(request):
    if request.method == "GET":
        form = PublishForm.objects.create()
        form = FormMakeSerializer(form)
        return JsonResponse(form.data, safe=False)


class PictureViewSet(viewsets.ModelViewSet):
    queryset = Picture.objects.all()
    serializer_class = PictureSerializer
    permission_classes = [AllowAny]

    def create(self, request, *args, **kwargs):
        file=request.data['pic']
        size=file.size
        response = super(ModelViewSet, self).create(request, *args, **kwargs)
        return response
