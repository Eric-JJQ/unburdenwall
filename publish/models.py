from django.db import models


# Create your models here.
class PublishForm(models.Model):
    fid = models.AutoField(primary_key=True)
    ip = models.GenericIPAddressField(default="0.0.0.0", blank=True, null=True)
    time = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    type = models.CharField(max_length=20, default="", blank=True, null=True)
    title = models.CharField(max_length=50, default="", blank=True, null=True)
    content = models.TextField(default="", blank=True, null=True)  # 小作文
    contact = models.CharField(max_length=50, default="", blank=True, null=True)
    status = models.CharField(max_length=50, default="", blank=True, null=True)

    class Meta:
        db_table = "PublishForm"
        ordering = ['-time']

    def __str__(self):
        return str(self.fid)+"|"+self.title


class Picture(models.Model):
    pid = models.AutoField(primary_key=True)
    fid = models.ForeignKey(to=PublishForm, on_delete=models.CASCADE,null=False,blank=False)
    pic = models.ImageField(upload_to='raw/', default="test.jpg",null=False,blank=False)  # 原图地址
    size = models.CharField(max_length=10, default="",blank=True, null=True)  # ...KB
    uptime = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = "Picture"
        ordering = ['-pid']

    def __str__(self):
        return str(self.fid) + "-" + str(self.pid)


class Thumb(models.Model):
    tid = models.AutoField(primary_key=True)
    pic = models.ForeignKey(to=Picture, on_delete=models.CASCADE)
    thumb = models.ImageField(upload_to='pics/thumb/', blank=True, null=True)
    size = models.CharField(max_length=10, default="")
    uptime = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = "Thumb"

    def __str__(self):
        return str(self.pic)
