# Generated by Django 3.2.4 on 2021-07-08 21:56

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('publish', '0003_alter_picture_thumb'),
    ]

    operations = [
        migrations.AlterField(
            model_name='picture',
            name='thumb',
            field=models.FilePathField(blank=True, default='test.jpg', null=True, path='pics/thumb/'),
        ),
    ]
