from django.contrib import admin

# Register your models here.
from publish.models import PublishForm, Picture, Thumb

admin.site.register(PublishForm)
admin.site.register(Picture)
admin.site.register(Thumb)