from django.utils import timezone
from rest_framework import serializers

from publish.models import PublishForm, Picture


class FormListSerializer(serializers.ModelSerializer):
    class Meta:
        model = PublishForm
        fields = [
            'fid',
            'ip',
            'time',
            'type',
            'title',
            'content',
            'contact',
            'status'
        ]


class FormMakeSerializer(serializers.Serializer):
    fid = serializers.IntegerField()

    class Meta:
        model = PublishForm
        fields = [
            'fid'
        ]

    def update(self, instance, validated_data):
        pass

    def create(self, validated_data):
        form = PublishForm.objects.create()
        fid = form.fid
        return form


class PictureSerializer(serializers.ModelSerializer):
    url = serializers.HyperlinkedIdentityField(view_name='picture-detail')

    class Meta:
        model = Picture
        fields = '__all__'

    def create(self, validated_data):
        print(validated_data)
        pic = Picture.objects.create(
            fid=validated_data['fid'],
            pic=validated_data['pic'],
            uptime=timezone.now()
        )
        pic.size = validated_data['pic'].size
        return pic
