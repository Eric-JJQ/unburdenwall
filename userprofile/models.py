from django.contrib.auth.models import User, AbstractUser
from django.db import models


# Create your models here.


class Checker(AbstractUser):
    # cid = models.OneToOneField(User,on_delete=models.CASCADE,primary_key=True)
    nickname = models.CharField(max_length=30, default="未设置昵称")
    phone = models.CharField(max_length=11, default="12345678901")
    qq = models.CharField(max_length=11, default="")

    def __str__(self):
        return self.username

    class Meta(AbstractUser.Meta):
        db_table = "checker"
